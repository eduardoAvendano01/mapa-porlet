import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { MapaPage } from '../pages/mapa/mapa';
import { CapturaPage } from '../pages/captura/captura';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { DomicilioService } from '../services/domicilioService';

import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MapaPage,
    CapturaPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    MapaPage,
    CapturaPage
  ],
  providers: [
    Geolocation,
    StatusBar,
    SplashScreen,
    {provide:DomicilioService,useClass:DomicilioService},
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
