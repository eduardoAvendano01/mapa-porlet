import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController, Platform, Events } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { DomicilioService } from '../../services/domicilioService';


declare var google;

/**
 * Generated class for the Mapa page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  componentForm: any;
  autocompleteInputValue: string;
  marker : any;
  isBrowser: boolean;

  @ViewChild('autocomplete') myElement: ElementRef;

  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation, private domicilioForm: DomicilioService, public alertCtrl: AlertController, public platform: Platform, public events: Events) {

    //Checar si es browser o device
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      this.isBrowser = false;
    } else {
      this.isBrowser = true;
    }

    this.componentForm = {
      postal_code: 'short_name', //codigo postal
      route: 'long_name', //nombre calle
      street_number: 'short_name', //numero calle
      administrative_area_level_1: 'long_name', //Estado
      locality: 'long_name', //Ciudad
      sublocality_level_1: 'short_name', //Colonia
      administrative_area_level_3: 'short_name' //Delegacion
    };

    window['LibMapaAllianz'] = { };
    window['LibMapaAllianz'].obtenerDireccionMapa = function () {
      console.log('obtenerDireccionMapa desde javascript');
      return this.valoresDireccion();
    }


    events.subscribe('tabMapaSelected', () => {
      console.log("En mapa event capturado");

      console.log("**autocompleteInputValue "+this.autocompleteInputValue);
      if( this.autocompleteInputValue !== null && this.autocompleteInputValue !== '' && this.autocompleteInputValue !== undefined){

        console.log("En if");

        this.loadMap2();
        this.autocomplete();


      //Obtenemos lat long del punto de places
        let latLng = new google.maps.LatLng(this.domicilioForm.latitudMapa, this.domicilioForm.longitudMapa);


        //Reasignando centro del mapa con el punto de places
         let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }
    
        
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        // google.maps.event.trigger(this.mapElement.nativeElement, "resize");

        /*

        google.maps.event.addListenerOnce(this.mapElement.nativeElement, 'idle', function() {
          google.maps.event.trigger(this.mapElement.nativeElement, 'resize');
          this.mapElement.nativeElement.setCenter(latLng);
        });*/

        //Creando marker
        var image = 'assets/img/allianz-pin.png';
        this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: this.map.getCenter(),
          icon: image
        });

        //Se agrega listener para click del marker
        google.maps.event.addListener(this.marker, 'click', () => {

            /*
            if (event.alreadyCalled_) {
                console.log('circle clicked again'); 
            }
            else {
                console.log('circle clicked first time');      
                event.alreadyCalled_ = true;
            }*/

            let alert = this.alertCtrl.create();
            alert.setTitle('Número Interior');
            alert.setSubTitle('Si tiene un número interior ingréselo, de otro modo sólo presione siguiente.');

            alert.addInput({
              type: 'input',
              label: 'Numero Interior:',
              value: '',
            });

            
            alert.addButton({
              text: 'Siguiente',
              handler: data => {
                console.log(' data:');
                console.log(data);

                this.domicilioForm.num_int = data[0];
                this.domicilioForm.fireTabSelectedEvent(); 
                this.navCtrl.parent.select(1);
              }
            });


            alert.present();
        });

      }


    });

  }

  ionViewDidLoad() {
    this.loadMap();
    this.autocomplete();
  }

  autocomplete(){
    let input_from = document.getElementById("autocomplete");

    let autocomplete = new google.maps.places.Autocomplete(input_from, {componentRestrictions: {country: "mx"}});
    google.maps.event.addListener(autocomplete, 'place_changed', () => {

       
        if( autocomplete.getPlace() !== null && autocomplete.getPlace() !== "" ){

        let place = autocomplete.getPlace();
        //this.latitude = place.geometry.location.lat();
        //this.longitude = place.geometry.location.lng();
        console.log(place);

        console.log("Latitud "+place.geometry.location.lat());
        console.log("Longitud "+place.geometry.location.lng());


        this.domicilioForm.latitudMapa = place.geometry.location.lat();
        this.domicilioForm.longitudMapa = place.geometry.location.lng();

        //Obtenemos lat long del punto de places
        let latLng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());


        //Reasignando centro del mapa con el punto de places
         let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }
    
        
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        // google.maps.event.trigger(this.mapElement.nativeElement, "resize");

        /*

        google.maps.event.addListenerOnce(this.mapElement.nativeElement, 'idle', function() {
          google.maps.event.trigger(this.mapElement.nativeElement, 'resize');
          this.mapElement.nativeElement.setCenter(latLng);
        });*/

        //Creando marker
        var image = 'assets/img/allianz-pin.png';
        this.marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: this.map.getCenter(),
          icon: image
        });

        //Se agrega listener para click del marker
        google.maps.event.addListener(this.marker, 'click', () => {

            /*
            if (event.alreadyCalled_) {
                console.log('circle clicked again'); 
            }
            else {
                console.log('circle clicked first time');      
                event.alreadyCalled_ = true;
            }*/

            let alert = this.alertCtrl.create();
            alert.setTitle('Número Interior');
            alert.setSubTitle('Si tiene un número interior ingréselo, de otro modo sólo presione siguiente.');

            alert.addInput({
              type: 'input',
              label: 'Numero Interior:',
              value: '',
            });

            
            alert.addButton({
              text: 'Siguiente',
              handler: data => {
                console.log(' data:');
                console.log(data);

                this.domicilioForm.num_int = data[0];
                this.domicilioForm.fireTabSelectedEvent(); 
                this.navCtrl.parent.select(1);
              }
            });


            alert.present();
        });

        //Limpiando valores de service
        this.domicilioForm.postal_code = '';
        this.domicilioForm.route = '';
        this.domicilioForm.street_number = '';
        this.domicilioForm.administrative_area_level_1 = '';
        this.domicilioForm.locality = '';
        this.domicilioForm.sublocality_level_1 = '';
        this.domicilioForm.administrative_area_level_3 = '';
        this.domicilioForm.num_int = '';

        //Iterando objeto place para obtener direccion estructurada
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (this.componentForm[addressType]) {
            var addressDetail = place.address_components[i][this.componentForm[addressType]];
            //console.log("Address detail "+addressDetail+" address type "+addressType);

            this.domicilioForm[addressType] = addressDetail;  
          }
        }

      }

        

      });
  }
 
  
  loadMap(){

    //Carga mapa con coordenadas default centro de cdmx
    let latLng = new google.maps.LatLng(this.domicilioForm.latitudMapa, this.domicilioForm.longitudMapa);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    //Obtengo coordenadas posicion y actualizo mapa
    this.geolocation.getCurrentPosition().then((position) => {

        this.domicilioForm.latitudMapa = position.coords.latitude;
        this.domicilioForm.longitudMapa = position.coords.longitude;

        let latLng = new google.maps.LatLng(this.domicilioForm.latitudMapa, this.domicilioForm.longitudMapa);

        let mapOptions = {
          center: latLng,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
    }, (err) => {
      console.log(err);
    });
 
  }


  loadMap2(){

    //Carga mapa con coordenadas default centro de cdmx
    let latLng = new google.maps.LatLng(this.domicilioForm.latitudMapa, this.domicilioForm.longitudMapa);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
  }

  blurAutocompleteInput(inputValue){

    //Si el campo es vacio elimino marker del mapa
    if( inputValue=== "" || inputValue === null){
        console.log("if vacio");

        this.marker.setMap(null);
    }


  }

  valoresDireccion(){
    console.log('En valoresDireccion, codigoPostal = ');
    return {
          valido: true,
          codigoPostal: this.domicilioForm.postal_code,
          nombreCalle: this.domicilioForm.route,
          numeroCalle: this.domicilioForm.street_number,
          numeroInterior: this.domicilioForm.num_int,
          estado: this.domicilioForm.administrative_area_level_1,
          ciudad: this.domicilioForm.locality,
          delegacion: this.domicilioForm.administrative_area_level_3,
          colonia: this.domicilioForm.sublocality_level_1
        };
  }

  

}
