import { Component } from '@angular/core';

import { CapturaPage } from '../captura/captura';
import { MapaPage } from '../mapa/mapa';

import { DomicilioService } from '../../services/domicilioService';



@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  mapaRoot = MapaPage;
  capturaRoot = CapturaPage;

  constructor(private domicilioForm: DomicilioService) {

  }

  capturaPageSelected(){
    this.domicilioForm.fireTabSelectedEvent();  
  }

  mapaPageSelected(){
    console.log("En mapa page selected");
    this.domicilioForm.fireMapaTabSelectedEvent();
  }
  
}
