import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import {App} from "ionic-angular";

import { DomicilioService } from '../../services/domicilioService';


/**
 * Generated class for the Captura page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-captura',
  templateUrl: 'captura.html',
})
export class CapturaPage {

  isBrowser: boolean;
  disableButton: string = "true";

  codigoPostal: string;
  nombreCalle: string;
  numeroCalle: string;
  estado: string;
  ciudad: string;
  colonia: string;
  delegacion: string;
  numeroInterior: string;

  domicilios: any;
  estados: any;
  municipiosCiudades: any;
  asentamientos: any;
  ciudades: any;

  codigoEstado : any = 9;
  codigoMunicipio : any = 1;
  codigoAsentamiento : any;
  codigoCiudad : any;

  tipoAsentamiento : any = 9; //9 Colonia

  //TODO agregar codigos colonia, etc

  //URL servicios catalogos  sepomex
  baseURLFindByCP: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogo.sepomex/findSepomexCodigoCP';
  baseURLFindEstados: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogos.estados/findEstados';
  baseURLFindCiudadMunicipio: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogos.sepomexestado/findSepomexEstadoMunicipioCiudad';
  baseURLFindAsentamiento: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogo.sepomex/findSepomexByCodigoEstadoMunicipioTipoAsentamiento';
  baseURLFindCiudad: string = 'http://54.159.19.174:8080/o/allianz-ws/catalogos.sepomexestado/findSepomexEstadoCiudad';

  constructor(public navCtrl: NavController, public navParams: NavParams, private domicilioForm: DomicilioService, public events: Events, public http: Http, public platform: Platform, public app: App) {

    this.tipoAsentamiento = 9;

    //Checar si es browser o device
    if(this.platform.is('core') || this.platform.is('mobileweb')) {
      console.log("Its not an app");
      this.isBrowser = false;
    } else {
      console.log("Its an an app");
      this.isBrowser = true;
    }

    //Copiando valores de domicilioService
    this.codigoPostal = domicilioForm.postal_code;
    this.nombreCalle = domicilioForm.route;
    this.numeroCalle = domicilioForm.street_number;
    this.estado = domicilioForm.administrative_area_level_1;
    this.ciudad = domicilioForm.locality;
    this.colonia = domicilioForm.sublocality_level_1;
    this.delegacion = domicilioForm.administrative_area_level_3;
    this.numeroInterior = domicilioForm.num_int;



    window['LibMapaAllianz'] = { };
    window['LibMapaAllianz'].obtenerDireccionMapa = function () {
      console.log('obtenerDireccionMapa desde javascript');
      return this.valoresDireccion();
    }


    if( this.codigoPostal !== '' && this.codigoPostal !== null){

        //Obtiene datos por cp
        this.http.get(this.baseURLFindByCP+"?codigo_cp="+this.domicilioForm.postal_code).map(res => res.json()).subscribe(data => {

          this.domicilios = data;
          this.codigoEstado = data[0].codigoEstado;
          this.codigoMunicipio = data[0].codigoMunicipio;
          this.codigoCiudad = data[0].ciudad;
          this.tipoAsentamiento = data[0].codigoTipoAsentamiento;
  

          //Obtiene municipios y ciudades por cp
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.municipiosCiudades = data;
          
          });

          //Obtiene asentamientos
          this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {

            this.asentamientos = data;

            for(var i=0 ; i<data.length ; i++){
              if( data[i].asentamiento === this.domicilioForm.sublocality_level_1 ){
                this.codigoAsentamiento = data[i].idAsentamientoCP;
                break;
              }

            }
          
          });

          //Obtiene ciudades
          this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {
            this.ciudades = data;              
          });


        });

        //Obtiene estados
        this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
          this.estados = data;
        });
        


      }else{


          /*
    
          //Obtiene estados
          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
            this.estados = data;
          });

           //Obtiene municipios y ciudades por codigo estado
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {
            this.municipiosCiudades = data; 
          });*/

          //console.log("En change estado "+this.codigoEstado);


          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
              this.estados = data;

              this.codigoEstado = data[0].codigoEstado;

              //Obtiene municipios y ciudades por estado
              this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {
                this.municipiosCiudades = data;
                this.codigoMunicipio = data[0].codigoMunicipio;

                //Obtiene asentamientos
                //Seteando valor de tipo de asentamiento
                this.tipoAsentamiento = 9;
                this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
                  this.asentamientos = data;
                  this.codigoAsentamiento = data[0].idAsentamientoCP;
                });

                //Obtiene ciudades
                this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

                  this.ciudades = data;  
                  this.codigoCiudad = data[0].ciudad;          
                
                });


              });


              

          });

        
      }

    
    //Listener evento tabCapturaSelected
    events.subscribe('tabCapturaSelected', () => {

      console.log("En tabs captura selected event");

      //Copiando valores de domicilio service
      this.codigoPostal = domicilioForm.postal_code;
      this.nombreCalle = domicilioForm.route;
      this.numeroCalle = domicilioForm.street_number;
      this.estado = domicilioForm.administrative_area_level_1;
      this.ciudad = domicilioForm.locality;
      this.colonia = domicilioForm.sublocality_level_1;
      this.delegacion = domicilioForm.administrative_area_level_3;
      this.numeroInterior = domicilioForm.num_int;


      //CARGA DE CATALOGOS SEPOMEX

      if( this.codigoPostal !== '' && this.codigoPostal !== null){
        console.log("En if codigo postal");
        console.log("Codigo postal "+this.domicilioForm.postal_code);

        //Obtiene datos por cp
        this.http.get(this.baseURLFindByCP+"?codigo_cp="+this.domicilioForm.postal_code).map(res => res.json()).subscribe(data => {

          this.domicilios = data;
          this.codigoEstado = data[0].codigoEstado;
          this.codigoMunicipio = data[0].codigoMunicipio;
          this.codigoCiudad = data[0].ciudad;
          this.tipoAsentamiento = data[0].codigoTipoAsentamiento;
  

          //Obtiene municipios y ciudades por estado
          this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.municipiosCiudades = data;
          
          });

          //Obtiene asentamientos
          this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {

            this.asentamientos = data;

            for(var i=0 ; i<data.length ; i++){
              if( data[i].asentamiento === this.domicilioForm.sublocality_level_1 ){
                this.codigoAsentamiento = data[i].idAsentamientoCP;
                break;
              }

            }
          
          });

          //Obtiene ciudades
          this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

            this.ciudades = data;            
          
          });


        });

        //Obtiene estados
        this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {
          console.log(data[0].descripcion);

          this.estados = data;
        });
        


      }else{

          //Limpia valores ingresados previamente
          this.codigoEstado = "";
          this.codigoMunicipio = "";
          this.codigoCiudad = "";
          this.codigoAsentamiento = "";
          this.tipoAsentamiento = "";
    
          this.http.get(this.baseURLFindEstados).map(res => res.json()).subscribe(data => {

            this.estados = data;
            this.codigoEstado = data[0].codigoEstado;

              //Obtiene municipios y ciudades por estado
              this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {
                this.municipiosCiudades = data;
                this.codigoMunicipio = data[0].codigoMunicipio;

                //Obtiene asentamientos
                //Seteando valor de tipo de asentamiento
                this.tipoAsentamiento = 9;
                this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
                  this.asentamientos = data;
                  this.codigoAsentamiento = data[0].idAsentamientoCP;
                });

                //Obtiene ciudades
                this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

                  this.ciudades = data;  
                  this.codigoCiudad = data[0].ciudad;          
                
                });
              });
          });
          
      }

      this.validarForma();

    });


    this.validarForma();

  }


  /*FUNCIONES*/

  
  blurCodigoPostal(){
    this.validarForma();
  }

  blurNombreCalle(){
    this.validarForma();
  }

  blurNumeroCalle(){
    this.validarForma();
  }

  blurNumeroInterior(){
    this.validarForma();
  }

  changeColonia(){
    console.log("En change colonia");
    this.validarForma();
  }


  changeDelegacionMunicipio(){
    console.log("En change municipio");

    //Obtiene asentamientos
    //Seteando valor de tipo de asentamiento
    console.log("Codigo estado "+this.codigoEstado+" cod municipio "+this.codigoMunicipio);
    this.tipoAsentamiento = 9; //Tipo: colonia
    this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
      this.asentamientos = data;
      this.codigoAsentamiento = data[0].idAsentamientoCP;
    });

    this.validarForma();

  }

  changeCiudad(){
    console.log("En change ciudad");

    this.validarForma();
  }

  changeEstado(){
    console.log("En change estado "+this.codigoEstado);

     //Obtiene municipios y ciudades por estado
    this.http.get(this.baseURLFindCiudadMunicipio+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {
      this.municipiosCiudades = data;
      this.codigoMunicipio = data[0].codigoMunicipio;

      //Obtiene asentamientos
      //Seteando valor de tipo de asentamiento
      this.tipoAsentamiento = 9;
      this.http.get(this.baseURLFindAsentamiento+"?codigo_estado="+this.codigoEstado+"&codigo_municipio="+this.codigoMunicipio+"&codigo_tipo_asentamiento="+this.tipoAsentamiento).map(res => res.json()).subscribe(data => {
        this.asentamientos = data;
        this.codigoAsentamiento = data[0].idAsentamientoCP;
      });


    });


    //Obtiene ciudades
    this.http.get(this.baseURLFindCiudad+"?codigoEstado="+this.codigoEstado).map(res => res.json()).subscribe(data => {

      this.ciudades = data;  
      this.codigoCiudad = data[0].ciudad;          
    
    });

    this.validarForma();
  }






  //Push a siguiente página del flujo
  siguientePagina(){
  }

  //Directiva codigo postal
  onKeyPress(e, codigoPostal){
      //console.log("entra a on keyup "+this.codigoPostal);
      if(this.codigoPostal.length>0){
        //console.log("model--"+this.codigoPostal );
        var res = this.codigoPostal.charAt(this.codigoPostal.length-1);
        //console.log("Antes de if "+res.match('[0-9.]'));
        if(!res.match('[0-9.]')){
          //console.log("antes de slice--"+this.codigoPostal );
          this.codigoPostal = this.codigoPostal.slice(0, -1);
          //console.log("despues de slice--"+this.codigoPostal );
        }
      }
      //console.log("antes");
      this.codigoPostal = this.codigoPostal.replace(/\D/g,'');
      //console.log("despues");
    }

    //Envio de datos portlet
    valoresDireccion(){
    console.log('En valoresDireccion, codigoPostal = ');
    return {
          valido: true,
          codigoPostal: this.domicilioForm.postal_code,
          nombreCalle: this.domicilioForm.route,
          numeroCalle: this.domicilioForm.street_number,
          numeroInterior: this.domicilioForm.num_int,
          estado: this.domicilioForm.administrative_area_level_1,
          ciudad: this.domicilioForm.locality,
          delegacion: this.domicilioForm.administrative_area_level_3,
          colonia: this.domicilioForm.sublocality_level_1
        };
   }


   validarForma(){

     console.log("En validar forma "+this.codigoPostal);

      if(this.codigoPostal !== '' && this.codigoPostal !== null && this.nombreCalle !== '' && this.nombreCalle !== null && this.numeroCalle !== '' && this.numeroCalle !== null){
        console.log("En if");
        this.disableButton  = "false";
      }else{
        this.disableButton  = "true";
      }

   }

}
